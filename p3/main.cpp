#include<iostream>
#include<vector>
#include<algorithm>
int really_binsearch(std::vector<int> &nums, int search_item, int start, int end);
int binsearch(std::vector<int> &nums, int search_items);
int main(void){
	int num_items;
	using namespace std;

	vector<int> nums;
	cin >> num_items;

	int tmp;//var for temporarily keeping the nums
	for (int i=0; i<num_items; ++i){
		cin >> tmp;
		nums.push_back(tmp);
	}

	//sorting the vector just in case
	cout << "sorting elements...\n";
	sort(nums.begin(), nums.end());
	int search_item;
	cin >> search_item;
	cout << "Working...\n";
	//printing out output
	cout << binsearch(nums, search_item)<< endl;
	return 0;
}
int binsearch(std::vector<int> &nums, int search_items){
	return really_binsearch(nums, search_items, 0, nums.size());
}
int really_binsearch(std::vector<int> &nums, int search_item, int start, int end){
	int mid;
	while(start<end){
		mid = (end-start)/2;
		if (nums[mid]<search_item){
			end = mid;
		}
		else if (nums[mid]>search_item){
			start = mid;
		}
		else{
			return mid;
		}
	}	
	return -1;
}
